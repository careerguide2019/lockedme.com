package com.simplilearn.lockme.application;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.simplilearn.lockme.model.UserCredentials;
import com.simplilearn.lockme.model.Users;

public class Authentication {
              //input data
              private static Scanner Keyboard;//console
              private static Scanner input;//file
              private static Scanner lockerinput;
              //output data
              private static PrintWriter output;
              private static PrintWriter lockerOutput;
              //model to store data
              private static Users users;
              private static UserCredentials userCredentials;
              public static void initApp() {
             File dbFile= new File("database.txt"); //dbFile is the pointer to read the file
             File lockerFile= new File("locker-file.txt"); //lockerFile is the pointer to read the file
                            
                             try {
                                           
                       //read data from dbfile
                   input=new Scanner(dbFile);
                                           
                      //read data from locker file
                   lockerinput=new Scanner(lockerFile);
                     //read data from keyboard
                  Keyboard=new Scanner(System.in);
               //output
           output =new PrintWriter(new FileWriter(dbFile, true));
           lockerOutput =new PrintWriter(new FileWriter(lockerFile, true));
                                        
                  users=new Users();
             userCredentials=new UserCredentials();
                                           
                             } catch (IOException e) {
                                           // TODO Auto-generated catch block
                                           System.out.println("404: File not found");
                             }
                            
              }

              
              public static void main(String[] args) {
                             
              initApp();            
              welcomeScreen();
              signInOptions();
              
                             
}
              public static void signInOptions() {
                    System.out.println("1. Registration");
                     System.out.println("2. Login");
                    int option=Keyboard.nextInt();
                       switch(option) {
         case 1:
              registerUser();
               break;
         case 2:
               loginUser();
               break;
          default :
               System.out.println("Please select 1 or 2");
               break;
                                          
                             }
            Keyboard.close();
            input.close();
              }
         
              public static void lockerOptions(String inpUserName) {
              System.out.println("1. FETCH ALL STORED CREDENTIALS");
             System.out.println("2. STORED CREDENTIALS");
               int option=Keyboard.nextInt();
               switch(option) {
        case 1:
             fetchcredentials(inpUserName);
             break;
        case 2:
             storeCredentials(inpUserName);
             break;
        default :
             System.out.println("Please select 1 or 2");
             break;
                                   
               }
                 Keyboard.close();
                 input.close();
              }
              public static void registerUser() {
                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
                System.out.println("*                                                                                                                                         ");
                System.out.println("*        WELCOME TO REGISTRATION SCREEN      *");
                System.out.println("*                                                                                                                                         ");
                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
                             System.out.println("Enter UserName:" );
                             String username=Keyboard.next();
                             users.setUsername(username);
                             System.out.println("Enter Password:" );
                             String password=Keyboard.next();
                             users.setPassword(password);
                             output.println(users.getUsername());
                             output.println(users.getPassword());
                             System.out.println("User Registration Successfull");
                             output.close();
                             
              }
              public static void loginUser() {
                  System.out.println("============================================");
                             System.out.println("*                                                                                                                                         ");
                             System.out.println("*       WELCOME TO LOGIN PAGE   *");
                             System.out.println("*                                                                                                                                         ");
                  System.out.println("============================================");
                             System.out.println("Enter UserName:" );
                             String inputusername=Keyboard.next();
                             boolean found=false;
                                           while(input.hasNext()&& !found)
                                           {
                    if(input.next().equals(inputusername)){
               System.out.println("Enter Password :");
                             String inpassword=Keyboard.next();
                             if(input.next().equals(inpassword)) {
               System.out.println("Login successful! 200");
                             found=true;
                             lockerOptions(inputusername);
                             break;
                                 }
                                 }
                                                          
                                 }
                   if(!found) {
              System.out.println("User Not  found: Login failure! 404");
                                 }
                                           
              }
              //store credentials
              public static void storeCredentials(String loggedInUser) {
                           System.out.println("********************************************");
                           System.out.println("*                                                                                                                                         ");
                           System.out.println("*  WELCOME TO DIGITAL LOCKER STORE YOUR CREDS HERE *");
                           System.out.println("*                                                                                                                                         ");
                           System.out.println("********************************************");
                             userCredentials.setLoggedInUser(loggedInUser);
                             
                             System.out.println("Enter Site Name:" );
                             String siteName=Keyboard.next();
                             userCredentials.setSiteName(siteName);
                             
                             System.out.println("Enter User Name:" );
                             String username=Keyboard.next();
                             userCredentials.setUsername(username);
                             
                             System.out.println("Enter Password:" );
                             String password=Keyboard.next();
                             userCredentials.setPassword(password);
                             
                             lockerOutput.println(userCredentials.getLoggedInUser());
                             lockerOutput.println(userCredentials.getSiteName());
                             lockerOutput.println(userCredentials.getUsername());
                             lockerOutput.println(userCredentials.getPassword());
                             
                             System.out.println("YOUR CREDS ARE STORED AND SECURED!");
              
                             lockerOutput.close();
                             
              }
              //fetch  credentials
              public static void  fetchcredentials(String inpUserName) {
                  System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
                             System.out.println("*                                                                                                                                         ");
                             System.out.println("*        WELCOME TO DIGITAL LOCKER *");
                             System.out.println("*        YOUR CREDENTIALS ARE      *");
                             System.out.println("*                                                                                                                                         ");
                  System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
                             
                             while(lockerinput.hasNext())
                             {
                             if(lockerinput.next().equals(inpUserName)){
                             System.out.println("Site Name:"+ lockerinput.next());
                             System.out.println("User Name:" +lockerinput.next());
                             System.out.println("Password :" +lockerinput.next());
                                               
                                                                        
                                                          }
                                           }
                             
                             
              }
              public static void welcomeScreen() {
                  System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
                             System.out.println("*                                                                                                                                         ");
                             System.out.println("*         Welcome to LockMe.com                                       ");
                             System.out.println("*    Your Personal Digital Locker                       ");
                             System.out.println("*                                                                                                                                         ");
                  System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
                             
              }
              
              
}
